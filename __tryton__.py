# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Export',
    'name_de_DE': 'Buchhaltung Export',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Export
    - Base module for different export methods from accounting

''',
    'description_de_DE': '''Buchhaltung Export
    - Basismodul für verschiedene Exportmethoden aus der Buchhaltung
''',
    'depends': [
        'account',
    ],
    'xml': [
        'export.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
