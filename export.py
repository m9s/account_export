#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard

_EXPORT_TYPES = []


class ExportInit(ModelView):
    'Export Init'
    _name = 'account.export.init'
    _description = __doc__

    type = fields.Selection(_EXPORT_TYPES, 'Export Type',
            required=True)

ExportInit()


class Export(Wizard):
    'Process Export'
    _name = 'account.export'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.export.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('choice', 'Next', 'tryton-ok', True),
                ],
            },
        },
        'choice': {
            'result': {
                'type': 'choice',
                'next_state': '_choice',
            },
        },
        'demo': {
            'result': {
                'type': 'action',
                'action': '_action_export_demo',
                'state': 'end',
            },
        },
    }

    def _choice(self, data):
        return data['form']['type']

    def _action_export_demo(self, data):
        return {}

Export()
